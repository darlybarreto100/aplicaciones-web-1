document.addEventListener('DOMContentLoaded', () => {
    const editButton = document.getElementById('editButton');
    const saveButton = document.getElementById('saveButton');
    const inputs = document.querySelectorAll('#userData input');
    const visitHistoryTable = document.getElementById('visitHistory').querySelector('tbody');
    const bonoStatusElement = document.getElementById('bonoStatus');

    // Simulación de datos del usuario
    const userData = {
        name: 'Juan Pérez',
        email: 'juan.perez@example.com',
        phone: '1234567890',
        Discapacidad: 'Visual',
        profilePicture: './Imagenes/Perfil.jpg',
        visitHistory: [
            { workerName: 'Ana Gómez', date: '2024-01-15', observations: 'Visita inicial.' },
            { workerName: 'Carlos Martínez', date: '2024-02-20', observations: 'Seguimiento mensual.' }
        ],
        bonoStatus: 'Último cobro realizado el 2024-03-10.'
    };

    // Función para cargar los datos del usuario
    function loadUserData() {
        document.getElementById('profilePicture').src = userData.profilePicture;
        document.getElementById('userName').innerText = userData.name;
        document.getElementById('name').value = userData.name;
        document.getElementById('email').value = userData.email;
        document.getElementById('phone').value = userData.phone;
        document.getElementById('Discapacidad').value = userData.Discapacidad

        visitHistoryTable.innerHTML = '';
        userData.visitHistory.forEach(visit => {
            const row = document.createElement('tr');
            row.innerHTML = `
                <td>${visit.workerName}</td>
                <td>${visit.date}</td>
                <td>${visit.observations}</td>
            `;
            visitHistoryTable.appendChild(row);
        });

        bonoStatusElement.innerText = userData.bonoStatus;
    }

    // Función para habilitar la edición de datos
    function enableEditing() {
        inputs.forEach(input => input.disabled = false);
        editButton.style.display = 'none';
        saveButton.style.display = 'block';
    }
    // Función para guardar los cambios
    function saveChanges() {
        userData.name = document.getElementById('name').value;
        userData.email = document.getElementById('email').value;
        userData.phone = document.getElementById('phone').value;
        userData.Discapacidad = document.getElementById('Discapacidad').value;
        loadUserData();
        inputs.forEach(input => input.abled = false);
        editButton.style.display = 'block';
        saveButton.style.display = 'none';
        
    }

    // Eventos
    editButton.addEventListener('click', enableEditing);
    saveButton.addEventListener('click', saveChanges);

    // Cargar datos al inicio
    loadUserData();
});
