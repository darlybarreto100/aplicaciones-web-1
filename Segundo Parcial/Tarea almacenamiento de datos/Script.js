function abrirRegistros() {
    window.open('Registros.html', '_blank');
}

function Validacion() {
    var Nombre = document.getElementById("nombre").value;
    var Apellido = document.getElementById("apellido").value;
    var Cedula = document.getElementById("ced").value;
    var Correo = document.getElementById("correo").value;
    var Telefono = document.getElementById("cel").value;
    var Direccion = document.getElementById("direccion").value;
    var Sexo = document.querySelector('input[name="Sexo"]:checked');
    var Estadocivil = document.querySelector('input[name="Estado"]:checked');

    var valid = true;

    // Validar campos vacíos
    if (Nombre == "") {
        alert("El campo nombre no debe estar vacío. \nComplete el campo.");
        valid = false;
    }

    if (Apellido == "") {
        alert("El campo apellido no debe estar vacío. \nComplete el campo.");
        valid = false;
    }

    if (Cedula == "") {
        alert("El campo cédula no debe estar vacío. \nComplete el campo.");
        valid = false;
    }

    if (Correo == "") {
        alert("El campo correo no debe estar vacío. \nComplete el campo.");
        valid = false;
    }

    if (Telefono == "") {
        alert("El campo teléfono no debe estar vacío. \nComplete el campo.");
        valid = false;
    }

    if (Direccion == "") {
        alert("El campo dirección no debe estar vacío. \nComplete el campo.");
        valid = false;
    }

    if (!Sexo) {
        alert("El campo sexo no debe estar vacío. \nComplete el campo.");
        valid = false;
    }

    if (!Estadocivil) {
        alert("El campo estado civil no debe estar vacío. \nComplete el campo.");
        valid = false;
    }

    // Validar cédula y teléfono numéricos y longitud
    if (isNaN(Cedula) || Cedula.length != 10) {
        alert("La cédula debe ser un número de 10 dígitos. \nRevise el campo.");
        valid = false;
    }

    if (isNaN(Telefono) || Telefono.length != 10) {
        alert("El teléfono debe ser un número de 10 dígitos. \nRevise el campo.");
        valid = false;
    }

    // Validar que Nombre y Apellido solo contengan letras
    var soloLetras = /^[a-zA-Z\s]+$/;
    if (!soloLetras.test(Nombre)) {
        alert("El campo nombre solo debe contener letras. \nRevise el campo.");
        valid = false;
    }

    if (!soloLetras.test(Apellido)) {
        alert("El campo apellido solo debe contener letras. \nRevise el campo.");
        valid = false;
    }

    // Validar correo
    var ValidacionCorreo = /^[\w._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    if (!ValidacionCorreo.test(Correo) || !Correo.includes("@") || !Correo.includes(".com") || Correo.split("@")[1].split(".")[0].length < 5) {
        alert("Correo no válido. \nIngrese un correo válido.");
        valid = false;
    }

    if (valid) {
        GUARDADO();
    }
}

function GUARDADO() {
    var Nombre = document.getElementById("nombre").value;
    var Apellido = document.getElementById("apellido").value;
    var Cedula = document.getElementById("ced").value;
    var Correo = document.getElementById("correo").value;
    var Telefono = document.getElementById("cel").value;
    var Direccion = document.getElementById("direccion").value;
    var Sexo = document.querySelector('input[name="Sexo"]:checked').value;
    var Estadocivil = document.querySelector('input[name="Estado"]:checked').value;

    var registros = JSON.parse(localStorage.getItem("Registros")) || [];

    // Verificar si la cédula ya existe
    if (registros.some(registro => registro.Cedula === Cedula)) {
        alert("La cédula ya está registrada. \nIngrese una cédula diferente.");
        return;
    }

    var nuevoRegistro = {
        Nombre: Nombre,
        Apellido: Apellido,
        Cedula: Cedula,
        Correo: Correo,
        Telefono: Telefono,
        Direccion: Direccion,
        Sexo: Sexo,
        Estadocivil: Estadocivil
    };

    registros.push(nuevoRegistro);
    localStorage.setItem("Registros", JSON.stringify(registros));

    alert("Los datos han sido enviados sin ningún problema, tenga Buen día.");

    document.getElementById("nombre").value = "";
    document.getElementById("apellido").value = "";
    document.getElementById("ced").value = "";
    document.getElementById("correo").value = "";
    document.getElementById("cel").value = "";
    document.getElementById("direccion").value = "";
    document.querySelector('input[name="Sexo"]:checked').checked = false;
    document.querySelector('input[name="Estado"]:checked').checked = false;
}





